﻿/*-----------------------------------------------------------
   <author>Pedro Miguel Pereira Pimenta</author>
   <email>mailto:a10206@alunos.ipca.pt</email>
   <author>Davide Marques Ferreira</author>
   <email>mailto:a10677@alunos.ipca.pt</email>
   <author>Ricardo Nuno Miranda</author>
   <email>mailto:a102932@alunos.ipca.pt</email>
   <date>14/12/2016 12:30:40 PM</date>
   <unit> Engenharia de Software & Integração de Sistemas de Informação </unit>
   <course> Licenciatura em Engenharia de Sistemas Informáticos </course>
   <university> IPCA - Instituto Politécnico do Cávado e do Áve </university>
   <summary>
        * This class implements the interface of the SocialNetwork Service" 
 * </summary>
------------------------------------------------------------*/

using System.ServiceModel;

namespace SocialNetworksDLL
{
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        void PostToPageFacebookInc(string message, string link);

        [OperationContract]
        void PostToPageFacebook(string mensagem, string urlImagem);

        [OperationContract]
        void PostToPageFacebookIncNoFoto(string mensagem);

        [OperationContract]
        void PostToTwitter(string mensagem, string link);

        [OperationContract]
        void PostToTwitterNoFoto(string mensagem);

        [OperationContract]
        bool TwitterPrivateMessage(string mensagem);
    }
}
