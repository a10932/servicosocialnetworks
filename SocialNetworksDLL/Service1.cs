﻿/*-----------------------------------------------------------
   <author>Pedro Miguel Pereira Pimenta</author>
   <email>mailto:a10206@alunos.ipca.pt</email>
   <author>Davide Marques Ferreira</author>
   <email>mailto:a10677@alunos.ipca.pt</email>
   <author>Ricardo Nuno Miranda</author>
   <email>mailto:a102932@alunos.ipca.pt</email>
   <date>14/12/2016 12:30:40 PM</date>
   <unit> Engenharia de Software & Integração de Sistemas de Informação </unit>
   <course> Licenciatura em Engenharia de Sistemas Informáticos </course>
   <university> IPCA - Instituto Politécnico do Cávado e do Áve </university>
   <summary>
        * This class implements the "SocialNetworkService" 
 * </summary>
------------------------------------------------------------*/

using System.Collections.Generic;
using System.Web.UI;
using Facebook;
using System.Net;
using System.IO;
using Tweetinvi;
using System.Diagnostics;
using Tweetinvi.Models;
using Tweetinvi.Parameters;

/// <summary>
/// Classe SocialNetworks
/// </summary>
namespace SocialNetworksDLL
{
    public class Service1 : Page, IService1
    {
        #region Facebook

        /// <summary>
        /// Método para postar no facebook.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="link"></param>
        public void PostToPageFacebookInc(string message, string link)
        {
            //EAACEdEose0cBAD4D6UGBILyW8YtU0k4ZB6ZA86GggDV4av6iZCX1ULfZCEoYhgUHlKRD3uZACvWmsP5gZBLMRKLp3gKZBujZAW4lqsLuyI7vLTC72eQQQU8PnGADZCCcb7pHqDVqwnkQh8usP0TDsRr0zvkXZA0k9IhzotSKZC2TEIbKgZDZD
            //EAADGxNBhTJ4BAMcIWZBKjUgYIBfDZAIYsGZBV4ATbCVrrEfz98ZB3lupIaMbgMHoH1WpEmK8FKK1lSICIOGSx5aqExshJFCWwOA17oLPFmtlqqFWZBnZCTHyKgshdfZCfwWqxQZCwMyIr270CzmwmstA2ljI317lOwEKY5vXkNA3hAZDZD
            //var fb = new FacebookClient("EAACEdEose0cBAAjNEC7azMsm2nSV5JBbqP6wVnuMGZAhpogYwILOKxGHMZAMXpbv69emZBI5SdXI6dXZCw8HboGjQvumIPx1JlskZCCLXQNZCUBXTMBJlPnSyhha9FUHQIswxn9vbaVqjD65jByURXtiIChlIAdJzbk03GRc9g5gZDZD");

            var fb = new FacebookClient();

            var argList = new Dictionary<string, object>();
            argList["message"] = message;
            argList["link"] = link;
            fb.AccessToken = "EAACEdEose0cBAGj2gdPfXD6Uolw1mWpyYtlt6811lxGl2GG9EVjHBGYD3m062ZClFhozzuhPRhG8wZBe5ggLkofMtgcgUvJRJqUeLdZAGUgNei7g0aIqntvgZCbSZBIDS9CavkZCPZCCkQ8sfDPwCgqHLPuVt6TjZC6IxxFOlDdDZBgZDZD";
            fb.AppSecret = "170476e1e1c27c2389f4f1b7cc004a73";
            fb.AppId = "218548611927198";
            fb.Post("/arrendasempre/feed", argList);
        }

        /// <summary>
        /// Método para postar no facebook sem foto
        /// </summary>
        /// <param name="message"></param>
        /// <param name="link"></param>
        public void PostToPageFacebookIncNoFoto(string message)
        {
            //EAACEdEose0cBAD4D6UGBILyW8YtU0k4ZB6ZA86GggDV4av6iZCX1ULfZCEoYhgUHlKRD3uZACvWmsP5gZBLMRKLp3gKZBujZAW4lqsLuyI7vLTC72eQQQU8PnGADZCCcb7pHqDVqwnkQh8usP0TDsRr0zvkXZA0k9IhzotSKZC2TEIbKgZDZD
            //EAADGxNBhTJ4BAMcIWZBKjUgYIBfDZAIYsGZBV4ATbCVrrEfz98ZB3lupIaMbgMHoH1WpEmK8FKK1lSICIOGSx5aqExshJFCWwOA17oLPFmtlqqFWZBnZCTHyKgshdfZCfwWqxQZCwMyIr270CzmwmstA2ljI317lOwEKY5vXkNA3hAZDZD
            //var fb = new FacebookClient("EAACEdEose0cBAAjNEC7azMsm2nSV5JBbqP6wVnuMGZAhpogYwILOKxGHMZAMXpbv69emZBI5SdXI6dXZCw8HboGjQvumIPx1JlskZCCLXQNZCUBXTMBJlPnSyhha9FUHQIswxn9vbaVqjD65jByURXtiIChlIAdJzbk03GRc9g5gZDZD");

            try
            {
                var fb = new FacebookClient();

                var argList = new Dictionary<string, object>();
                argList["message"] = message;
                fb.AccessToken = "EAACEdEose0cBAGj2gdPfXD6Uolw1mWpyYtlt6811lxGl2GG9EVjHBGYD3m062ZClFhozzuhPRhG8wZBe5ggLkofMtgcgUvJRJqUeLdZAGUgNei7g0aIqntvgZCbSZBIDS9CavkZCPZCCkQ8sfDPwCgqHLPuVt6TjZC6IxxFOlDdDZBgZDZD";
                fb.AppSecret = "170476e1e1c27c2389f4f1b7cc004a73";
                fb.AppId = "218548611927198";
                fb.Post("/arrendasempre/feed", argList);
            }
            catch (System.Exception)
            {
            }

        }

        /// <summary>
        /// Método para postar no facebook
        /// NOTA: Inicialmente desenvolvido em ASP.NET
        /// </summary>
        /// <param name="mensagem">mensagem a publicar</param>
        /// <param name="urlImagem">url da imagem a publicar</param>
        public void PostToPageFacebook(string mensagem, string urlImagem)
        {
            string appID = "218548611927198";
            string appSecret = "170476e1e1c27c2389f4f1b7cc004a73";

            //Dictionary para guarda a mensagem e os links para a(s) imagem(ns)
            var argList = new Dictionary<string, object>();

            //string scope = "publish_actions,manage_pages";
            string scope = "publish_actions,manage_pages";

            if (Request["code"] == null)
            {
                Response.Redirect(string.Format(
                    "https://graph.facebook.com/oauth/authorize?client_id={0}&redirect_uri={1}&scope={2}", appID, Request.Url.AbsoluteUri, scope));
            }
            else
            {
                Dictionary<string, string> tokens = new Dictionary<string, string>();

                //Url "pré-definido" pelo Facebook
                string url = string.Format(
                    "https://graph.facebook.com/oauth/access_token?client_id={0}&redirect_uri={1}&scope={2}&code={3}&client_secret={4}", appID, Request.Url.AbsoluteUri, scope, Request["code"].ToString(), appSecret);
                //https://graph.facebook.com/oauth/access_token?

                HttpWebRequest request = WebRequest.Create(url.ToString()) as HttpWebRequest;

                //ler a resposta e "trazer" os tokens
                using (HttpWebResponse responde = request.GetResponse() as HttpWebResponse)
                {
                    StreamReader reader = new StreamReader(responde.GetResponseStream());
                    string vals = reader.ReadToEnd();

                    foreach (string token in vals.Split('&'))
                    {
                        tokens.Add(token.Substring(0, token.IndexOf("=")),
                            token.Substring(token.IndexOf("=") + 1, token.Length - token.IndexOf("=") - 1));
                    }
                }

                string access_token = tokens["access_token"];

                //Token Client
                var client = new FacebookClient(access_token);

                //Mensagem e url das imagens
                argList["message"] = mensagem;
                argList["link"] = urlImagem;

                //Post no facebook
                client.Post("arrendasempre/feed", argList);


            }
        }

        #endregion

        #region Twitter

        /// <summary>
        /// Método para postar no twitter
        /// </summary>
        /// <param name="mensagem">mensagem</param>
        /// <param name="link">link</param>
        public void PostToTwitter(string mensagem, string link)
        {
            //SetAuth();

            string consumerKey = "1dlja4qzUUwx7758jUtwxFptd";
            string consumerSecret = "Qs5e9HxgPq0CZ1KdGK0CYUB7eKaj9k4aUV6MxjbTPu3vpVDD6M";
            string token = "704820698871685120-rMHYprq7ZKhzof5aIrhtuKsQZYwFGVi";
            string tokenSecret = "hvbFWb4fcseMgCEqPRd1467MyxcHwdUzKjWwQt0WcMfQv";

            Auth.SetUserCredentials(consumerKey, consumerSecret, token, tokenSecret);

            //var user = User.GetAuthenticatedUser();

            byte[] img = File.ReadAllBytes(link);
            var media = Upload.UploadImage(img);

            var tweet = Tweet.PublishTweet(mensagem, new PublishTweetOptionalParameters
            {
                Medias = new List<IMedia> { media }
            });

            Process.Start(tweet.Url.ToString());
        }

        /// <summary>
        /// Método para postar no twitter sem foto
        /// </summary>
        /// <param name="mensagem">mensagem</param>
        /// <param name="link">link</param>
        public void PostToTwitterNoFoto(string mensagem)
        {
            //SetAuth();

            string consumerKey = "1dlja4qzUUwx7758jUtwxFptd";
            string consumerSecret = "Qs5e9HxgPq0CZ1KdGK0CYUB7eKaj9k4aUV6MxjbTPu3vpVDD6M";
            string token = "704820698871685120-rMHYprq7ZKhzof5aIrhtuKsQZYwFGVi";
            string tokenSecret = "hvbFWb4fcseMgCEqPRd1467MyxcHwdUzKjWwQt0WcMfQv";

            Auth.SetUserCredentials(consumerKey, consumerSecret, token, tokenSecret);

            //var user = User.GetAuthenticatedUser();

            var tweet = Tweet.PublishTweet(mensagem);

            Process.Start(tweet.Url.ToString());
        }


        /// <summary>
        /// Envia uma mensagem privada para a página do twitter da imobiliaria
        /// </summary>
        /// <param name="mensagem"></param>
        /// <returns></returns>
        public bool TwitterPrivateMessage(string mensagem)
        {
            //SetAuth();
            string consumerKey = "1dlja4qzUUwx7758jUtwxFptd";
            string consumerSecret = "Qs5e9HxgPq0CZ1KdGK0CYUB7eKaj9k4aUV6MxjbTPu3vpVDD6M";
            string token = "704820698871685120-rMHYprq7ZKhzof5aIrhtuKsQZYwFGVi";
            string tokenSecret = "hvbFWb4fcseMgCEqPRd1467MyxcHwdUzKjWwQt0WcMfQv";

            try
            {
                TwitterCredentials creds = new TwitterCredentials(consumerKey, consumerSecret, token, tokenSecret);
                //Auth.SetUserCredentials(consumerKey, consumerSecret, token, tokenSecret);
                Auth.SetUserCredentials(creds.ConsumerKey, creds.ConsumerSecret, creds.AccessToken, creds.AccessTokenSecret);

                var message = Message.PublishMessage(mensagem, "bideferreira96");

                if (message.IsMessagePublished) return true;
            }
            catch (System.Exception)
            {

                return false;
            }

            return false;
        }

        private void SetAuth()
        {
            string consumerKey = "1dlja4qzUUwx7758jUtwxFptd";
            string consumerSecret = "Qs5e9HxgPq0CZ1KdGK0CYUB7eKaj9k4aUV6MxjbTPu3vpVDD6M";
            string token = "704820698871685120-rMHYprq7ZKhzof5aIrhtuKsQZYwFGVi";
            string tokenSecret = "hvbFWb4fcseMgCEqPRd1467MyxcHwdUzKjWwQt0WcMfQv";

            Auth.SetUserCredentials(consumerKey, consumerSecret, token, tokenSecret);
        }

        #endregion
    }
}
